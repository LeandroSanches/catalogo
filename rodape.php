</div>
</div>
<footer class="page-footer fixed-footer">
	<div class="footer-copyright">
		<div class="container">
			<div class="row">
				<small>
					<div class="col s12 m4" align="center">© 2018 Copyright</div>
					<div class="col s12 m4" align="center">Ultima atualização da página
						<?php  echo $date = $date = str_replace('-', '/', $date);?> -
						<?= $hora?>
					</div>
					<!--<div class="col s12 m4" align="center">Seu ID:
						<?= $usuario['id']; ?>
					</div>-->
				</small>
			</div>
		</div>
	</div>
</footer>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<!--<script src="js/jquery-1.4.3.min.js"></script>
<script src="js/materialize.min.js"></script>-->
<script type="text/javascript" src="js/acao.js"></script>
<script type="text/javascript" src="js/jquery.mask.js"></script>
<script>
	
	$( document ).ready( function () {
		
		$( ".msg" ).delay( 5000 ).fadeOut( 1000 );

		// the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
		$( '.modal' ).modal();

		// Initialize collapse button
		$( ".button-collapse" ).sideNav();

		$( 'select' ).material_select();

		var options =  {
		  onKeyPress: function(cep, e, field, options) {
			var masks = ['00/00/0000'];
			var mask = (cep.length>8) ? masks[1] : masks[0];
			$('.dataDeProducao').mask(mask, options);
		}};

		//$( '.crazy_cep' ) . mask( '00/00/0000', options );

		$('.dropdown-button').dropdown({
		  inDuration: 300,
		  outDuration: 225,
		  constrainWidth: false, // Does not change width of dropdown to that of the activator
		  hover: true, // Activate on hover
		  gutter: 0, // Spacing from edge
		  belowOrigin: false, // Displays dropdown below the button
		  alignment: 'left', // Displays dropdown with edge aligned to the left of button
		  stopPropagation: false // Stops event propagation
		}
		);	


	} );
</script>
</html>