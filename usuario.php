<?php
require_once( "logica-usuario.php" );


$acao = $_GET[ 'acao' ];

if ( empty( $_GET[ 'id' ] ) ) {
	//header( "Location: usuarios.php" );
	?>
    <script>
	window.location.replace("usuarios.php");
	</script>
	<?php
}

if ( !empty( $_GET[ 'id' ] ) ) {
	$id = $_GET[ 'id' ];
}


if ( $acao == 'alterar' ) {	
	$id = $_POST[ 'id' ];
	$nome = $_POST[ 'nome' ];	
	$apelido = $_POST[ 'apelido' ];	
	$email = $_POST[ 'email' ];	
	$array = array($id, $nome, $apelido, $email); 
	
	if ( alterarUsuario( $conexao, $array ) ) {
		$msg = mysqli_error( $conexao );

		$_SESSION[ "danger" ] = 'Erro ao Alterar Usuario.<br>
									Erro:' . $msg . '
								</div>';
		//header( "Location: usuarios.php" );
		?>
		<script>
		window.location.replace("usuarios.php");
		</script>
		<?php
	}
}

if ( $acao == 'bloq' ) {
	if ( bloquearUsuario( $conexao, $id ) ) {
		$msg = mysqli_error( $conexao );

		$_SESSION[ "danger" ] = 'Erro ao Bloquear Usuario.<br>
									Erro:' . $msg . '
								</div>';
		//header( "Location: usuarios.php" );
		?>
		<script>
		window.location.replace("usuarios.php");
		</script>
		<?php
	} else {
		$_SESSION[ "success" ] = 'Usuario Bloqueado.';
		//header( "Location: usuarios.php" );
		?>
		<script>
		window.location.replace("usuarios.php");
		</script>
		<?php
	}

}

if ( $acao == 'hab' ) {
	if ( habilitarUsuario( $conexao, $id ) ) {
		$msg = mysqli_error( $conexao );

		$_SESSION[ "danger" ] = 'Erro ao Habilitar Usuario.<br>
									Erro:' . $msg . '
								</div>';
		//header( "Location: usuarios.php" );
		?>
		<script>
		window.location.replace("usuarios.php");
		</script>
		<?php
	} else {

		$_SESSION[ "success" ] = 'Usuario Habilitar.';
		//header( "Location: usuarios.php" );
		?>
		<script>
		window.location.replace("usuarios.php");
		</script>
		<?php

	}
}


if ( $acao == 'sen' ) {
	$id = $_POST[ 'id' ];
	$senha1 = $_POST[ 'senha1' ];
	$senha2 = $_POST[ 'senha2' ];

	if ( !empty( $senha1 ) && !empty( $senha2 ) ) {
		if ( $senha1 === $senha2 ) {
			if ( mudarSenhaUsuario( $conexao, $id, $senha2 ) ) {
				$msg = mysqli_error( $conexao );

				$_SESSION[ "danger" ] = 'Erro ao alteradar senha.<br>
									Erro:' . $msg . '
								</div>';
				//header( "Location: usuarios.php" );
		?>
		<script>
		window.location.replace("usuarios.php");
		</script>
		<?php
			} else {

				$_SESSION[ "success" ] = 'Senha alterada com sucesso.';
				//header( "Location: usuarios.php" );
		?>
		<script>
		window.location.replace("usuarios.php");
		</script>
		<?php

			}
		}
	} else {
		$_SESSION[ "danger" ] = 'Senhas diferente ou em branco. Digite novamente.';
		//header( "Location: usuarios.php" );
		?>
		<script>
		window.location.replace("usuarios.php");
		</script>
		<?php
	}
}


if ( $acao == 'criar' ) {	
	$nome = $_POST[ 'nome' ];	
	$apelido = $_POST[ 'apelido' ];	
	$email = $_POST[ 'email' ];	
	$senha = $_POST[ 'senha' ];	
	
	if ( !empty( $nome ) && !empty( $apelido ) && !empty( $email ) && !empty( $senha ) ) {
			if ( criarUsuario( $conexao,  $nome, $apelido, $email, $senha) ) {
				$msg = mysqli_error( $conexao );

				$_SESSION[ "danger" ] = 'Erro ao criar usuario.<br>
									Erro:' . $msg . '
								</div>';
				//header( "Location: usuarios.php" );
		?>
		<script>
		window.location.replace("usuarios.php");
		</script>
		<?php
			} else {

				$_SESSION[ "success" ] = 'Usuario criado com sucesso.';
				//header( "Location: usuarios.php" );
		?>
		<script>
		window.location.replace("usuarios.php");
		</script>
		<?php

			}
	} else {
		$_SESSION[ "danger" ] = 'Campo em branco. Cadastre novamente.';
		//header( "Location: usuarios.php" );
		?>
		<script>
		window.location.replace("usuarios.php");
		</script>
		<?php
	}
}