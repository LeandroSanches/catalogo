<?php
require_once( "cabecalho.php" );
require_once( "banco-disciplina.php" );

$id = $_GET[ 'id' ];
$disciplina = buscarDisciplina( $conexao, $id );

?>

<!-- Inicio Disciplina -->
<div id="disciplina">
	<ul id="tabs-swipe-demo" class="tabs">
		<li class="tab col s6"><a href="#test-swipe-1">Alterando Disciplina</a>
		</li>
	</ul>
	<div id="test-swipe-1" class="white padding15">
		<div class="row ">
			<div class="col s12">
				<form action="altera-disciplina.php" method="post" class="text-left">
					<input type="hidden" name="id" value="<?=$disciplina['id']?>"/>
					<?php include("formulario-base-disciplina.php"); ?>
					<br/>
					<button class="btn btn-primary btn-block" type="submit">Alterar Disciplina</button>
				</form>

			</div>
		</div>
	</div>
</div>

<?php include("rodape.php"); ?>