<div class="row">

	<div class="input-field col s12 m9">
		<input placeholder="Ex.: Bacharelado em Sistemas de Informação" id="nome" type="text" class="validate" value="<?=$curso['nome']?>" name="nome">
		<label for="nome">Nome</label>
	</div>
	<div class="input-field col s12 m3">
		<input placeholder="Ex.: EGN-2018" id="codigo" type="text" class="validate" value="<?=$curso['codigo']?>" name="codigo">
		<label for="codigo">Codigo</label>
	</div>
</div>
<div class="row">

	<?php if($curso['datadecriacao'] != ""){?>
	<div class="input-field col s12 m3">
		<input disabled id="datadecriacao" name="datadecriacao" type="text" class="validate" value="<?=$curso['datadecriacao']?>" data-mask="0000/00/00 00:00:00">
		<label for="datadecriacao">Data de Criacao</label>
	</div>
	<?php } ?>

	<?php if($curso['datademodificacao'] != ""){?>
	<div class="input-field col s12 m3">
		<input disabled id="datademodificacao" name="datademodificacao" type="text" class="validate" value="<?=$curso['datademodificacao']?>" data-mask="00/00/0000">
		<label for="datademodificacao">Data de Modificação</label>
	</div>
	<?php } ?>

	<?php if($curso['versao'] != ""){?>
	<!--<div class="input-field col s12 m1">
		<input disabled id="versao" name="versao" type="text" class="validate" value="<?=$curso['versao']?>" >
		<label for="versao">Versão</label>
	</div>-->
	<?php } ?>

	<div class="input-field col s12">
		<textarea id="descricao" class="materialize-textarea" placeholder="Ex.: Esta disciplina ... " name="descricao">
			<?=$curso['descricao']?>
		</textarea>
		<label for="descricao">Descrição</label>
	</div>



</div>