<?php require_once("conecta.php");

// Verifica se foi feita alguma busca
// Caso contrario, redireciona o visitante pra home
if (empty($_GET['unidade'])) {
	header("Location: /catalogo/index.php");
}

$consulta = $_GET['unidade'];

// Conecte-se ao MySQL antes desse ponto
// Salva o que foi buscado em uma variável
$busca = mysqli_real_escape_string($conexao, $consulta);
//$busca = $_GET['consulta'];
// ============================================
// Monta outra consulta MySQL para a busca
$query = "SELECT * 
			FROM unidade
				WHERE (desativado = 0) 
					AND ((nome LIKE '%$busca%'))";
// Executa a consulta
$resultado = mysqli_query($conexao, $query);
// ============================================
// Começa a exibição dos resultados

$unidade = array();
while($result = mysqli_fetch_assoc($resultado)) {
        array_push($unidade, $result);
    }

