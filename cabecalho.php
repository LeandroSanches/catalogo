<?php
error_reporting( E_ALL ^ E_NOTICE );

header( "Content-Type: text/html; charset=ISO-8859-1", true );
header( "Content-type: text/html; charset=utf-8" );


include_once( "mostra-alerta.php" );
require_once( "logica-usuario.php" );

date_default_timezone_set( 'America/Sao_Paulo' );
$date = date( 'd-m-Y' );
$hora = date( 'H:i:s' );

/**
 * @author Leandro Sanches <leandrosancheslima@gmail.com>
 */

?>
<html>

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">

	<title>Catalogo Unigranrio</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="icon" href="img/GloboUnigranrio.png">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/css/materialize.min.css">
	
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	
	<link rel="stylesheet" href="css/materialize.css">

	<link rel="stylesheet" type="text/css" href="css/styler.css"/>

	<style>
		.barPesquisar {
			position: absolute;
			width: 20%;
			height: 3rem;
			left: 40%;
			margin-top: 1rem;
			color: #56ccf2;
		}
		
		.barPesquisar button{
			position: absolute; 
			top: -13px; 
			right: -15%; 
			background-color: transparent; 
			border: 0px;
			color: #56ccf2;
		}
	</style>

</head>

<body>
	<section class="menu">

		<div class="navbar-fixed">
			<nav>
				<div class="nav-wrapper">
					<a href="" class="brand-logo">
						<center><img src="img/logo.png" class="responsive-img " width="60%" style="margin-top: 1rem;"/>
						</center>
					</a>
					<?php if(usuarioEstaLogado()) { ?>
					<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons" style="color: #56ccf2;">menu</i></a>
					
					<ul class="right hide-on-med-and-down">
					<div class="barPesquisar">
						<form method="GET" action="index.php">
							<input type="text" id="consulta" name="unidade" maxlength="20" class="center-align" title="Pesquisar Unidade" />
							
							<button type="submit" title="Pesquisar" >
								<i class="fa fa-search tiny"></i>
							</button>
							
						</form>
					</div>
						<li><a href="index.php">Lista</a>
						</li>
						<?php if($usuario['nivel'] > 0){ ?>
						<li><a href="adicionar.php">Adicionar</a>
						</li>
						<?php } ?>
						<li><a href="usuarios.php">Usuarios</a>
						</li>						
						<li><a href="sobre.php">Sobre</a>
						</li>
						<li><a href="logout.php"><i class="fas fa-sign-out-alt"></i></a>
						</li>
					</ul>
					<ul class="side-nav  blue-grey lighten-4" id="mobile-demo">
						<div style="background-image: url(img/Blurred-Lights-Background-02.jpg); background-position: bottom; background-size: cover; padding: 30px;">
							<li>
								<h5>Minha conta: <? $usuario = getUsuario($usuario, $conexao, $emailUsuarioAtual);?></h5>
							</li>
							<li>Nome:
								<?= $usuario['nome']?>
							</li>
						</div>
						<li><a href="index.php">Home</a>
						</li>
						<li><a href="adicionar.php">Adicionar</a>
						</li>						
						<li><a href="usuarios.php">Usuarios</a>
						</li>						
						<li><a href="sobre.php">Sobre</a>
						</li>				
						<li><a href="logout.php"><i class="fas fa-sign-out-alt"></i></a>
						</li>
					</ul>
					<?php } ?>
				</div>
			</nav>
		</div>
	</section>


	<div class="container">

		<div class="principal">

			<?php
			mostrarAlerta( "success" );
			mostrarAlerta( "danger" );
			?>