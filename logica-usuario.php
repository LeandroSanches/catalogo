<?php require_once("banco-usuario.php");

session_start();

/**@var string $emailUsuarioAtual | Guarda o Email do usuario logado no momento*/
$emailUsuarioAtual = usuarioLogado();
$usuario = buscarDadosUsuario($conexao, $emailUsuarioAtual);

function getUsuario($usuario, $conexao, $emailUsuarioAtual){
	$emailUsuarioAtual = usuarioLogado();
	$usuario = buscarDadosUsuario($conexao, $emailUsuarioAtual);
	return $usuario;
}

function usuarioEstaLogado() {
    return isset($_SESSION["usuario_logado"]);
}

function verificarUsuario() {
	
  if(!usuarioEstaLogado()) {
      $_SESSION["danger"] = "Você não tem acesso a esta funcionalidade.";
     ?>
    <script>
	window.location.replace("index.php");
	</script>
	<?php
     die();
  }
}

function usuarioLogado() {
	/**
      * @Function: usuarioLogado();
      * @Description: Esta funcao envia o email usado pelo usuario;
	  * @Return String; 
      */	
	return $_SESSION["usuario_logado"];    
}

function logarUsuario($email) {
    $_SESSION["usuario_logado"] = $email;
}

function logout() {	
	/**
      * @Function: logout();
      * @Description: Esta funcao desloga o usuario;
      */
    session_destroy();
    session_start();
}

function estadoDaConta($estado){
	$estado = $estado;
	
	if($estado == 0){
		return "Ativada";
	}
	if($estado != 0){
		return "Desativada";
	}
}

function bloquearUsuario($conexao, $id){
	bloquearUsuarioBD($conexao, $id);
}
function habilitarUsuario($conexao, $id){
	habilitarUsuarioBD($conexao, $id);
}

function mudarSenhaUsuario( $conexao, $id, $senha2 ){
	mudarSenhaUsuarioBD($conexao, $id, $senha2);
}

function criarUsuario( $conexao,  $nome, $apelido, $email, $senha){
	criarUsuariobd($conexao, $nome, $apelido, $email, $senha);
}

function alterarUsuario( $conexao, $array ){
	alterarUsuarioBD( $conexao, $array );
}

function listarUsuario( $conexao, $id ){
	return listarUsuariobd( $conexao, $id );
}

