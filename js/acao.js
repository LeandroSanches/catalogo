var tempo = 600;

var tempoIn = tempo;
var tempoOut = tempo;
var tempoDelay = tempo;

function mostrarListarCurso(){
  esconderListarDisciplina();
  esconderListarUnidade();
  esconderListarArea();
  esconderListarEscola();
  $("#curso").delay(tempoDelay).fadeIn(tempoIn);
}

function esconderListarCurso(){
  $("#curso").fadeOut(tempoOut);
}

function mostrarListarDisciplina(){
  esconderListarCurso();
  esconderListarUnidade();
  esconderListarArea();
  esconderListarEscola();
  $("#disciplina").delay(tempoDelay).fadeIn(tempoIn);
}

function esconderListarDisciplina(){
  $("#disciplina").fadeOut(tempoOut);
}

function mostrarListarUnidade(){
  esconderListarCurso();
  esconderListarDisciplina();
  esconderListarArea();
  esconderListarEscola();
  $("#unidade").delay(tempoDelay).fadeIn(tempoIn);
}

function esconderListarUnidade(){
  $("#unidade").fadeOut(tempoOut);
}

function mostrarListarArea(){
  esconderListarCurso();
  esconderListarDisciplina();
  esconderListarUnidade();
  esconderListarEscola();
  $("#area").delay(tempoDelay).fadeIn(tempoIn);
}

function esconderListarArea(){
  $("#area").fadeOut(tempoOut);
}

function mostrarListarEscola(){
  esconderListarCurso();
  esconderListarDisciplina();
  esconderListarUnidade();
  esconderListarArea();
  $("#escola").delay(tempoDelay).fadeIn(tempoIn);
}

function esconderListarEscola(){
  $("#escola").fadeOut(tempoOut);
}

function mostrarAdicionarQuestao(){
  esconderListarQuestao();
  $("#adicionarQuestao").delay(tempoDelay).fadeIn(tempoIn);
}

function esconderAdicionarQuestao(){
  $("#adicionarQuestao").fadeOut(tempoOut);
}

function mostrarListarQuestao(){
  esconderAdicionarQuestao();
  $("#listarQuestao").delay(tempoDelay).fadeIn(tempoIn);
}

function esconderListarQuestao(){
  $("#listarQuestao").fadeOut(tempoOut);
}


/*materialize jQuery*/ 
$(".button-collapse").sideNav();
$('select').material_select();
