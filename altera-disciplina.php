<?php require_once("cabecalho.php");
      require_once("banco-disciplina.php");

$id = $_POST["id"];
$nome = $_POST["nome"];
$descricao = $_POST["descricao"];
$tipo = $_POST["tipo"];
$versao = $_POST["versao"];

if(empty($tipo)){
	//Grad
	//$tipo = 'GRD';
	$tipo = 1;
}
if($tipo === 'on'){
	//Grad
	//$tipo = 'POS';
	$tipo = 2;
}

if(alterarDisciplina($conexao, $id, $nome, $descricao, $tipo)) { ?>
    <p class="text-success">O produto <?= $nome; ?>, alterado com sucesso!</p>
<?php 
	$_SESSION["success"] = "Disciplina alterada com sucesso."; 
	?>
    <script>
	window.location.replace("index.php");
	</script>
	<?php 
} else {
    $msg = mysqli_error($conexao);
?>
    <p class="text-danger">O produto <?= $nome; ?> não foi alterado: <?= $msg ?></p>
<?php
}
?>

<?php include("rodape.php"); ?>
