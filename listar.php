<?php
require_once( "cabecalho.php" );

/*****************************************/

require_once( "logica-usuario.php" );
verificarUsuario();
require_once( "logica-unidade.php" );
require_once( "logica-disciplina.php" );
require_once( "logica-curso.php" );

/*****************************************/

?>

<div id="unidade">
	<ul id="tabs-swipe-demo" class="tabs">
		<li class="tab col s6"><a href="#test-swipe-1">Unidades</a>
		</li>
		<li class="tab col s6"><a href="#test-swipe-2">Disciplina</a>
		</li>
		<li class="tab col s6"><a href="#test-swipe-3">Curso</a>
		</li>
	</ul>
	<div id="test-swipe-1" class="white padding15">
		<div class="row ">
			<div class="col s12">
				<?php if (empty($_GET['unidade'])) {						
					$unidade = listarUnidade( $conexao );					
					}
					
					if (!empty($_GET['unidade'])) {
						require_once( "busca.php" );
					}
					
					if (empty($unidade)){
						?>
				<center>
					<strong>Nenhuma Unidade Encontrada</strong>
				</center> <br>
				<a class="btn" href="index.php"> <i class="material-icons Tiny">reply</i> Voltar</a>
				<?php
				} else {
					?>
				<table class="striped table-bordered">
					<!--<thead>
						<tr>						
							<td><b>Nome:</b>
							</td>
							<td>
							</td>
						</tr>
					</thead>-->
					<?php foreach ( $unidade as $unidade ): ?>
					<tr>
						<td>
							<a href="<?=$unidade['link']?>" target="_blank">
								<?php $strlen = strlen($unidade['id']);	
														switch ($strlen) {
															case 1:
																$cod =  "<small>UND_000".$unidade['id']."</small> ";
																break;
															case 2:
																$cod = "<small>UND_00".$unidade['id']."</small> ";
																break;
															case 3:
																$cod = "<small>UND_0".$unidade['id']."</small> ";
																break;
															case 4:
																$cod = "<small>UND_".$unidade['id']."</small> ";
																break;
														}
									echo substr($unidade['nome'], 0, 60)." (".$cod.")";
									?>
							</a>
						</td>
						<td>
							<!-- Dropdown Trigger -->
							<a class='dropdown-button' href='#' data-activates='dropdown<?=$qtd?>'><i class="material-icons">add</i></a>
							<!-- Dropdown Structure -->
							<ul id='dropdown<?= $qtd++ ?>' class='dropdown-content'>
								<li><a href="<?=$unidade['link']?>" target="_blank">Visualizar</a>
								</li>
								<?php if($usuario['nivel'] == 1){ ?>
								<li><a href="altera-formulario-unidade.php?id=<?=$unidade['id']?>">Alterar</a>
								</li>
								<li><a class="red-text" href="excluir-unidade.php?id=<?=$unidade['id']?>">Deletar</a>
								</li>
								<?php } ?>
							</ul>
						</td>
					</tr>
					<?php endforeach ; 
					}
					?>
				</table>
			</div>
		</div>
	</div>
	<div id="test-swipe-2" class="white padding15">
		<div class="row ">
			<div class="col s12">
				<?php if (empty($_GET['disciplina'])) {						
					$disciplina = listarDisciplinasBD($conexao);				
					}
					
					if (!empty($_GET['disciplina'])) {
						require_once( "busca.php" );
					}
					
					if (empty($disciplina)){
						?>
				<center>
					<strong>Nenhuma Unidade Encontrada</strong>
				</center> <br>
				<a class="btn" href="index.php"> <i class="material-icons Tiny">reply</i> Voltar</a>
				<?php
				} else {
					?>
				<table class="striped table-bordered">
					<!--<thead>
						<tr>						
							<td><b>Nome:</b>
							</td>
							<td>
							</td>
						</tr>
					</thead>-->
					<?php foreach ( $disciplina as $disciplina ): ?>
					<tr>
						<td width="93%">
								<?php  $strlen = strlen($disciplina['id']);	
														switch ($strlen) {
															case 1:
																$cod =  "000".$disciplina['id'];
																break;
															case 2:
																$cod = "00".$disciplina['id'];
																break;
															case 3:
																$cod = "0".$disciplina['id'];
																break;
															case 4:
																$cod = $disciplina['id'];
																break;																
														}	
						
									if($disciplina['tipo'] == 1){
										$tipo = "GRD_";
									}
									if($disciplina['tipo'] == 2){
										$tipo = "POS_";
									}
						
								 echo substr($disciplina['nome'], 0, 60)." (<small>".$tipo.$cod."</small>)";	?>
						</td>
						<td>
							<!-- Dropdown Trigger -->
							<a class='dropdown-button' href='#' data-activates='dropdown<?=$qtd?>'><i class="material-icons">add</i></a>
							<!-- Dropdown Structure -->
							<ul id='dropdown<?= $qtd++ ?>' class='dropdown-content'>								
								<?php if($usuario['nivel'] == 1){ ?>
								<li><a href="altera-formulario-disciplina.php?id=<?=$disciplina['id']?>">Alterar</a>
								</li>
								<li><a class="red-text" href="excluir-disciplina.php?id=<?=$disciplina['id']?>">Deletar</a>
								</li>
								<?php } ?>
							</ul>
						</td>
					</tr>
					<?php endforeach ; 
					}
					?>
				</table>
			</div>
		</div>
	</div>
	<div id="test-swipe-3" class="white padding15">
		<div class="row ">
			<div class="col s12">
				<?php if (empty($_GET['curso'])) {						
					$curso = listarCursoBD($conexao);				
					}
					
					if (!empty($_GET['curso'])) {
						require_once( "busca.php" );
					}
					
					if (empty($curso)){
						?>
				<center>
					<strong>Nenhum Curso Encontrado</strong>
				</center> <br>
				<a class="btn" href="index.php"> <i class="material-icons Tiny">reply</i> Voltar</a>
				<?php
				} else {
					?>
				<table class="striped table-bordered">
					
					<?php foreach ( $curso as $curso ): ?>
					<tr>
						<td width="93%">
								<?php echo substr($curso['nome'], 0, 60)." (<small>".$curso['codigo']."</small>)";	?>
						</td>
						<td>
							<!-- Dropdown Trigger -->
							<a class='dropdown-button' href='#' data-activates='dropdown<?=$qtd?>'><i class="material-icons">add</i></a>
							<!-- Dropdown Structure -->
							<ul id='dropdown<?= $qtd++ ?>' class='dropdown-content'>								
								<?php if($usuario['nivel'] == 1){ ?>
								<li><a href="altera-formulario-curso.php?id=<?=$curso['id']?>">Alterar</a>
								</li>
								<li><a class="red-text" href="excluir-curso.php?id=<?=$curso['id']?>">Deletar</a>
								</li>
								<?php } ?>
							</ul>
						</td>
					</tr>
					<?php endforeach ; 
					}
					?>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- Final Unidade -->


<?php require_once("rodape.php"); ?>