<div class="row">	
	
	<div class="input-field col s12 m9">
		<input placeholder="Ex.: Redes Multisserviços" id="nome" type="text" class="validate" value="<?=$disciplina['nome']?>" name="nome">
		<label for="nome">Nome</label>
	</div>	
	<?php if($disciplina['tipo'] === 1){
		$checked = "checked";
	} else{
		$checked = "";
	}?>
	<div class="input-field col s12 m3">
		<!-- Switch -->
		  <div class="switch" align="center">
			<label>
			  Grad
			  <input type="checkbox" name="tipo" <?=$checked?>>
			  <span class="lever"></span>
			  Pos
			</label>
		  </div>
	</div>
	</div>
<div class="row">

	<?php if($disciplina['datadecriacao'] != ""){?>
	<div class="input-field col s12 m3">
		<input disabled  id="datadecriacao" name="datadecriacao" type="text" class="validate" value="<?=$disciplina['datadecriacao']?>" data-mask="0000/00/00 00:00:00">
		<label for="datadecriacao">Data de Criacao</label>
	</div>
	<?php } ?>
	
	<?php if($disciplina['datademodificacao'] != ""){?>
	<div class="input-field col s12 m3">
		<input disabled  id="datademodificacao" name="datademodificacao" type="text" class="validate" value="<?=$disciplina['datademodificacao']?>" data-mask="00/00/0000">
		<label for="datademodificacao">Data de Modificação</label>
	</div>
	<?php } ?>

	<div class="input-field col s12">
		<textarea id="descricao" class="materialize-textarea" placeholder="Ex.: Esta disciplina ... " name="descricao">
			<?=$disciplina['descricao']?>
		</textarea>
		<label for="descricao">Descrição</label>
	</div>
	
	

</div>