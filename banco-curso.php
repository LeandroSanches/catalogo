<?php require_once("conecta.php");

function listarCursoBD($conexao) {
    $curso = array();
	
		$query = 'SELECT *
	FROM curso
		WHERE desativado = 0';
		
    $resultado = mysqli_query($conexao, $query);
	
    while($result = mysqli_fetch_assoc($resultado)) {
        array_push($curso, $result);
    }
	
    return $curso;
}

function inserirCurso( $conexao, $nome, $codigo, $descricao ) {
    $query = "insert into curso (codigo, nome,  descricao)
        values ('$codigo', '$nome', '$descricao')";
	
    return mysqli_query($conexao, $query);
}

function buscarCurso($conexao, $id) {
    $query = "select * from curso where id = {$id}";
    $resultado = mysqli_query($conexao, $query);
    return mysqli_fetch_assoc($resultado);
}

function alterarCurso($conexao, $array) {	
    $query = "update curso set nome = '{$array[1]}', codigo = '{$array[2]}',  descricao = '{$array{3}}', datademodificacao = '{$array[4]}'  where id = {$array[0]}";
    return mysqli_query($conexao, $query);
}

function removerCurso($conexao, $id){
	$query = "update curso set desativado = '1' where id = {$id}";
    return mysqli_query($conexao, $query);
}
