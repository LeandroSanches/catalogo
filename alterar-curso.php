<?php require_once("banco-curso.php");

$id = $_POST["id"];
$nome = $_POST["nome"];
$codigo = $_POST["codigo"];
$descricao = $_POST["descricao"];

date_default_timezone_set( 'America/Sao_Paulo' );
$date = date( 'd-m-Y' );
$hora = date( 'H:i:s' );
$datademodificacao =  $date. ' '. $hora;

$array = array($id, $nome, $codigo, $descricao,  $datademodificacao);

if(alterarCurso($conexao, $array)) {
    echo '<p class="text-success">O produto <?= $nome; ?>, alterado com sucesso!</p>';

	$_SESSION["success"] = "Disciplina alterada com sucesso."; 
	
    echo '<script>
	window.location.replace("index.php");
	</script>';
	
} else {
    $msg = mysqli_error($conexao);
    echo '<p class="text-danger">O produto <?= $nome; ?> não foi alterado: <?= $msg ?></p>';
}