<?php
      require_once("conecta.php");

function listarArea($conexao) {
    $area = array();
    $query = 'SELECT * FROM area';
    $resultado = mysqli_query($conexao, $query);
    while($result = mysqli_fetch_assoc($resultado)) {
      array_push($area, $result);
    }
    return $area;
}

function inserirArea($conexao, $nome, $descricao) {
    $query = "insert into area (nome, descricao)
        values ('{$nome}', '{$descricao}')";
    return mysqli_query($conexao, $query);
}


function buscarArea($conexao, $id) {
    $query = "select * from area where id = {$id}";
    $resultado = mysqli_query($conexao, $query);
    return mysqli_fetch_assoc($resultado);
}

function alterarArea($conexao, $id, $nome, $descricao) {
    $query = "update area set nome = '{$nome}', descricao = '{$descricao}' where id = {$id}";
    return mysqli_query($conexao, $query);
}
