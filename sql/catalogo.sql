-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 11-Maio-2018 às 19:02
-- Versão do servidor: 5.7.17
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `catalogo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `area`
--

INSERT INTO `area` (`id`, `nome`, `descricao`, `datadecriacao`) VALUES
(1, 'Direito', '', '2018-02-09 18:31:19'),
(2, 'Educação e Humanas', '', '2018-02-09 18:31:19'),
(3, 'Gestão', '', '2018-02-09 18:31:19'),
(4, 'Saúde', '', '2018-02-09 18:31:19'),
(5, 'Tecnologia', '', '2018-02-09 18:31:29');

-- --------------------------------------------------------

--
-- Estrutura da tabela `unidade`
--

CREATE TABLE `unidade` (
  `id` int(4) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `descricao` text NOT NULL,
  `datadeproducao` int(8) NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datademodificacao` varchar(20) DEFAULT NULL,
  `versao` float NOT NULL DEFAULT '1',
  `desativado` tinyint(1) NOT NULL DEFAULT '0',
  `id_area` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `unidade`
--

INSERT INTO `unidade` (`id`, `nome`, `link`, `descricao`, `datadeproducao`, `datadecriacao`, `datademodificacao`, `versao`, `desativado`, `id_area`) VALUES
(17, 'Impostos e Contribuições', 'unidade\\UND_0017', 'Foi', 20082018, '2018-02-07 22:45:55', '16-04-2018 16:06:56', 1, 1, 1),
(18, 'Impostos e Contribuições', 'unidade\\UND_0018', 'Foi', 20122017, '2018-02-07 22:47:22', '16-04-2018 16:07:12', 1, 0, 2),
(19, 'Demonstração das Mutações do Patrimônio Líquido', 'unidade/UND_0019/index.html', 'Vamos conhecer, a seguir, a diferença entre Reserva de Contingência e Passivo Contingente.\r\n\r\nDentro da DMPL (ou ainda da DLPA) e do Patrimônio Líquido, existe uma conta chamada Reserva de Contingência, assim como existem os Passivos Contingentes.\r\n\r\nMas qual será a diferença entre eles? É o que veremos a seguir!\r\n\r\nA Reserva de Contingência é uma conta de PL constituída para que a empresa possa compensar, no futuro, uma eventual diminuição do lucro decorrente de perda julgada provável, com base em um valor estimado. Esses valores são cíclicos e, caso não ocorram, devem ser revertidos para o lucro e calculados novamente para o ano seguinte.\r\n\r\nJá no caso do Passivo Contingente, os exemplos mais comuns são aqueles relacionados com processos judiciais, já que, nos casos em que a empresa possui a possibilidade de ter que indenizar alguém, já poderá constituir uma parcela no passivo para tal.\r\n\r\nNo entanto, as Reservas de Contingência possuem como base o lucro líquido, e as provisões são baseadas em uma conta de resultado, ou seja, em uma despesa. Em outro ponto de vista, as reservas ocorrem com base em algo do futuro, já as provisões em fatos do passado que ainda não se concretizaram, mas que possuem potencial para tal.				', 22022018, '2018-04-16 18:33:47', '16-04-2018 20:47:48', 1, 0, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `apelido` varchar(200) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `senha` varchar(200) NOT NULL,
  `datadecriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nivel` int(1) NOT NULL DEFAULT '0',
  `desativado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `apelido`, `nome`, `email`, `senha`, `datadecriacao`, `nivel`, `desativado`) VALUES
(1, 'Leandro', 'Leandro Baptista', 'leandrosancheslima@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2017-06-26 20:37:46', 1, 0),
(2, 'Teste', 'Conte de Teste', 'teste', 'e10adc3949ba59abbe56e057f20f883e', '2017-06-26 20:37:51', 0, 1),
(3, 'Leandro', 'Leandro Baptista', 'leandro.baptista@unigranrio.com.br', '969b5482f068ebb4dac55d19452b7042', '2017-10-23 21:18:31', 1, 0),
(4, 'Kfe', 'Leandro Rangel', 'lrangel@unigranrio.com.br', '969b5482f068ebb4dac55d19452b7042', '2017-11-23 23:23:35', 0, 0),
(5, 'Zane', 'Suzane', 'susane.bonifacio@unigranrio.com.br', '969b5482f068ebb4dac55d19452b7042', '2017-11-24 15:03:21', 0, 0),
(6, 'tst', 'Teste', 'teste@unigranrio.com.br', 'e10adc3949ba59abbe56e057f20f883e', '2018-05-04 15:18:38', 0, 0),
(7, 'Seu merda', 'Bruno Fioravanti', 'bruno.barbosa@unigranrio.com.br', '969b5482f068ebb4dac55d19452b7042', '2018-05-07 18:39:59', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unidade`
--
ALTER TABLE `unidade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `unidade`
--
ALTER TABLE `unidade`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
