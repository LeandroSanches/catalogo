<?php
require_once( "cabecalho.php" );
require_once( "banco-curso.php" );

$id = $_GET[ 'id' ];
$curso = buscarCurso( $conexao, $id );

?>

<!-- Inicio Unidade -->
<div id="curso">
	<ul id="tabs-swipe-demo" class="tabs">
		<li class="tab col s6"><a href="#test-swipe-1">Alterando Curso</a>
		</li>
	</ul>
	<div id="test-swipe-1" class="white padding15">
		<div class="row ">
			<div class="col s12">
				<form action="alterar-curso.php" method="post" class="text-left">
					<input type="hidden" name="id" value="<?=$curso['id']?>"/>
					<?php include("formulario-base-curso.php"); ?>
					<br/>
					<button class="btn btn-primary btn-block" type="submit">Alterar Curso</button>
				</form>

			</div>
		</div>
	</div>
</div>

<?php include("rodape.php"); ?>