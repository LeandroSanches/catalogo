<?php require_once("cabecalho.php");

if(usuarioEstaLogado()) { ?>
<!--  <div class="row"><br>
    <h4 align="center" class="light-green-text text-darken-3">Bem vindo <?= $usuario['apelido']; ?>!</h4>    
  </div>
  -->
  <?php require_once("listar.php") ?>

<?php } else { ?>
<div class="row valign-wrapper">
  <div class="col s12 m6 offset-m3">
	<div class="card">
        <div class="card-content">
			<h2 class="card-title"><strong>Login</strong></h2>
			<form action="logica-login.php" method="post">
				<div class="row">
					<div class="input-field col s12">
					  <input id="email" type="email" class="validate" name="email">
					  <label for="email">E-mail</label>
					</div>				
					<div class="input-field col s12">
					  <input id="password" type="password" class="validate" name="senha">
					  <label for="password">Senha</label>
					</div>								
					<div class="col s12">
						<button class="btn" type="submit">Entrar</button>
					</div>
				</div>
			</form>
	  	</div>
	</div>  
  </div>
</div>
<?php } ?>

<?php include("rodape.php"); ?>
