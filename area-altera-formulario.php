<?php require_once("cabecalho.php");
      require_once("banco-area.php");

$id = $_GET['id'];
$area = buscarArea($conexao, $id);?>

<h1 class="text-capitalize">Alterando Area</h1>
<form action="altera-area.php" method="post" class="text-left">
  <input type="hidden" name="id" value="<?=$area['id']?>" />
  <?php include("area-formulario-base.php"); ?>
  <br />
  <button class="btn btn-primary btn-block" type="submit">Alterar</button>
</form>

<?php include("rodape.php"); ?>
