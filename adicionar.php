<?php
require_once( "cabecalho.php" );

/*****************************************/

require_once( "logica-usuario.php" );

/*****************************************/

require_once( "logica-area.php" );
require_once( "logica-unidade.php" );

/****************************************/
?>
<!-- Inicio Unidade -->
<div id="unidade">
	<ul id="tabs-swipe-demo" class="tabs">
		<li class="tab col s6"><a href="#test-swipe-1">Unidade</a>
		</li>
		<li class="tab col s6"><a href="#test-swipe-2">Disciplina</a>
		</li>
		<li class="tab col s6"><a href="#test-swipe-3">Curso</a>
		</li>
	</ul>
	<div id="test-swipe-1" class="white padding15">
		<div class="row ">
			<div class="col s10 offset-s1">
				<form action="adicionar-unidade.php" method="post" enctype="multipart/form-data" class="text-left" accept-charset="ISO-8859-1">
					<?php require_once("formulario-base-unidade.php"); ?>

					<div class="row">
						<div class="input-field col s12">
							<input name="arquivo" type="file" id="fileToUpload">
						</div>
					</div>
					<button class="btn btn-block" type="submit">Cadastrar Unidade <i class="material-icons right">send</i> </button>
				</form>
			</div>
		</div>
	</div>
	<div id="test-swipe-2" class="white padding15">
		<div class="row ">
			<div class="col s10 offset-s1">
				<form action="adicionar-disciplina.php" method="post" enctype="multipart/form-data" class="text-left" accept-charset="ISO-8859-1">
					<?php require_once("formulario-base-disciplina.php"); ?>
					<button class="btn btn-block" type="submit">Cadastrar Disciplina <i class="material-icons right">send</i> </button>
				</form>
			</div>
		</div>
	</div>
	<div id="test-swipe-3" class="white padding15">
		<div class="row ">
			<div class="col s10 offset-s1">
				<form action="adicionar-curso.php" method="post" class="text-left">
					<?php require_once("formulario-base-curso.php"); ?>
					<button class="btn btn-block" type="submit">Cadastrar Curso <i class="material-icons right">send</i> </button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Final Unidade -->

<?php require_once("rodape.php"); ?>