<?php
session_start();

function mostrarAlerta($tipo) {
    if(isset($_SESSION[$tipo])) { ?>
       <div class="row">
        <p class=" col offset-m3 m6 center <?= $tipo ?>"><?= $_SESSION[$tipo]?></p>
       </div>
<?php
        unset($_SESSION[$tipo]);
    }
}
