<?php require_once ("banco-usuario.php");
      require_once("logica-usuario.php");

$usuario = buscarUsuario($conexao, $_POST["email"], $_POST["senha"]);

if($usuario == null) {
    $_SESSION["danger"] = "Usuário ou senha inválido.";
    //header("Location: index.php");?>
    <script>
	window.location.replace("index.php");
	</script>
	<?php	
} else {
    $_SESSION["success"] = "Usuário logado com sucesso.";
    logarUsuario($usuario["email"]);
    //header("Location: index.php");?>
    <script>
	window.location.replace("index.php");
	</script>
	<?php
}
die();
