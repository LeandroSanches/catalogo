<?php require_once("conecta.php");

function listarDisciplinasBD($conexao) {
    $disciplina = array();
	
		$query = 'SELECT *
	FROM disciplina
		WHERE desativado = 0';
		
    $resultado = mysqli_query($conexao, $query);
	
    while($result = mysqli_fetch_assoc($resultado)) {
        array_push($disciplina, $result);
    }
	
    return $disciplina;
}

function inserirDisciplina( $conexao, $tipo, $nome, $descricao ) {
    $query = "insert into disciplina ( tipo, nome,  descricao)
        values ( '$tipo','$nome', '$descricao')";
    return mysqli_query($conexao, $query);
}

function buscarDisciplina($conexao, $id) {
    $query = "select * from disciplina where id = {$id}";
    $resultado = mysqli_query($conexao, $query);
    return mysqli_fetch_assoc($resultado);
}

function alterarDisciplina($conexao, $id, $nome, $descricao, $tipo) {	
    $query = "update disciplina set nome = '{$nome}', descricao = '{$descricao}', tipo = '{$tipo}'  where id = {$id}";
    return mysqli_query($conexao, $query);
}

function removerDisciplina($conexao, $id){
	$query = "update disciplina set desativado = '1' where id = {$id}";
    return mysqli_query($conexao, $query);
}
