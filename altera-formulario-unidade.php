<?php
require_once( "cabecalho.php" );
require_once( "banco-area.php" );
require_once( "banco-unidade.php" );

$id = $_GET[ 'id' ];
$unidade = buscarUnidade( $conexao, $id );

$area = listarArea( $conexao );
?>

<!-- Inicio Unidade -->
<div id="unidade">
	<ul id="tabs-swipe-demo" class="tabs">
		<li class="tab col s6"><a href="#test-swipe-1">Alterando Unidade</a>
		</li>
	</ul>
	<div id="test-swipe-1" class="white padding15">
		<div class="row ">
			<div class="col s12">
				<form action="altera-unidade.php" method="post" class="text-left">
					<input type="hidden" name="id" value="<?=$unidade['id']?>"/>
					<?php include("formulario-base-unidade.php"); ?>
					<br/>
					<button class="btn btn-primary btn-block" type="submit">Alterar Unidade</button>
				</form>

			</div>
		</div>
	</div>
</div>

<?php include("rodape.php"); ?>