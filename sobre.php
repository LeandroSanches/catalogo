<?php
require_once( "cabecalho.php" );

/*****************************************/

require_once( "logica-unidade.php" );
require_once( "logica-usuario.php" );


verificarUsuario();

?>

<div class="row valign-wrapper">
  <div class="col s12 m6 offset-m3">
	<div class="card">
        <div class="card-content">
			<h2 class="card-title" align="center"><strong>Sobre</strong></h2>
			<p>Sistema em Desenvolvimento, consiste em catalogar as unidades de aprendizagens.</p> 
			<p>Versão Completa:  <a href="http://leandrobaptista.com.br">Leandro Baptista</a></p> 
			
			<h2 class="card-title">Creditos:</h2> 
			<p>Stakeholders: Leandro Rangel</p>
			<p>Designer de Interface: Suzane Bonifácio</p> 
			<p>Front-end / Back-end (Desenvolvimento): <a href="http://leandrobaptista.com.br">Leandro Baptista</a></p>
	  	</div>
	</div>  
  </div>
</div>

<?php require_once("rodape.php"); ?>