<?php
require_once( "cabecalho.php" );
require_once( "banco-usuario.php" );

?>


<div id="usuarios">
	<ul id="tabs-swipe-demo" class="tabs">
		<li class="tab col s5"><a href="#test-swipe-1">Lista de usuarios</a>
		</li>
		<?php if($usuario['nivel'] == 1 ){ ?>
		<li class="tab col s5"><a href="#test-swipe-2">Cadastrar</a>
		</li>
		<?php } ?>
	</ul>
	<div id="test-swipe-1" class="white padding15">
		<div class="row">
			<?php $usuarios = listarUsuarios( $conexao );
			foreach ( $usuarios as $usuarios ){					
				if($usuarios['desativado'] == 0){$color = 'green darken-1';}
				else {$color = 'deep-orange darken-1';}	
				
				if($usuario['nivel'] == 0 || $usuario['nivel'] > 1){
					
					if($usuarios['id'] == $usuario['id']){?>
			<div class="col s12">
				<div class="card <?=$color?>">
					<div class="card-content white-text">
						<span class="card-title">
							<?=$usuarios['id']?>- 
							<?=$usuarios['nome']?>
						</span>
						Apelido:
						<?=$usuarios['apelido']?>
						<p>E-mail:
							<?=$usuarios['email']?>
						</p>

						<p>Estado:
							<?php $estado = $usuarios['desativado'];
									echo estadoDaConta($estado); 
									?>
						</p>
						<p><small>Data de Criação: <?=$usuarios['datadecriacao']?></small>
						</p>
					</div>
					<div class="card-action">
						<a href="alterar-usuario.php?id=<?=$usuarios['id']?>">Alterar</a>
					</div>
				</div>

				<!-- Modal Structure -->
				<div id="modal1" class="modal">
					<div class="modal-content">
						<h4 align="center">Mudar senha</h4>

						<form method="post" action="usuario.php?acao=sen">
							<p>Digite sua nova senha:</p>
							<input type="text" name="id" value="<?=$usuarios['id']?>" class="hide"/>
							<input type="password" name="senha1"/>
							<p>Digite novamente sua nova senha:</p>
							<input type="password" name="senha2"/>
							<div class="modal-footer">
								<button class="btn waves-effect waves-green btn-flat white-text" type="submit">Alterar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<?php }
				}else{	?>

			<div class="col s12 m6">
				<div class="card <?=$color?>">
					<div class="card-content white-text">
						<span class="card-title">
							<?=$usuarios['id']?>-
							<?=$usuarios['nome']?>
						</span>
						Apelido:
						<?=$usuarios['apelido']?>
						<p>E-mail:
							<?=$usuarios['email']?>
						</p>

						<p>Estado:
							<?php $estado = $usuarios['desativado'];
									echo estadoDaConta($estado); 
									?>
						</p>
						<p><small>Data de Criação: <?=$usuarios['datadecriacao']?></small>
						</p>
					</div>
					<div class="card-action">
						<a href="alterar-usuario.php?id=<?=$usuarios['id']?>">Alterar</a>
						<?php if($usuario['nivel'] == 1 ){ ?>
						<?php if($usuarios['desativado'] == 0 ){?>
						<a href="usuario.php?acao=bloq&id=<?=$usuarios['id']?>">Bloquear</a>
						<?php } else {?>
						<a href="usuario.php?acao=hab&id=<?=$usuarios['id']?>">Habilitar</a>
						<?php } ?>
						<a class="modal-trigger" href="#modal1">Mudar&nbsp;Senha</a>
						<?php } ?>
					</div>
				</div>

				<!-- Modal Structure -->
				<div id="modal1" class="modal">
					<div class="modal-content">
						<h4 align="center">Mudar senha</h4>

						<form method="post" action="usuario.php?acao=sen">
							<p>Digite sua nova senha:</p>
							<input type="text" name="id" value="<?=$usuarios['id']?>" class="hide"/>
							<input type="password" name="senha1"/>
							<p>Digite novamente sua nova senha:</p>
							<input type="password" name="senha2"/>
							<div class="modal-footer">
								<button class="btn waves-effect waves-green btn-flat white-text" type="submit">Alterar</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<?php } } ?>
		</div>
	</div>
	<?php if($usuario['nivel'] == 1){?>
	<div id="test-swipe-2" class="white padding15">
		<div class="row">

			<form action="usuario.php?acao=criar" method="post" class="text-left">
				<?php require_once( "usuario-formulario-base.php" ); ?><br/>
				<button class="btn btn-primary btn-block" type="submit">Alterar Unidade</button>
			</form>
		</div>
	</div>
	<?php } ?>
	
</div>
</div>


<?php include("rodape.php"); ?>