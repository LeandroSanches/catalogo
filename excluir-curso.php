<?php require_once("cabecalho.php");
      require_once("banco-curso.php");
      require_once("logica-usuario.php");

verificarUsuario();

$id = $_GET["id"];

if(removerCurso($conexao, $id)) { 
	$_SESSION["success"] = 'O Curso foi Removido com sucesso!';	?>
    <script>
	window.location.replace("index.php");
	</script>
	<?php
	} else {
    $msg = mysqli_error($conexao);
	
	$_SESSION["success"] = '
								Atenção: O Curso <strong>não</strong> foi Removido !<br>
								Erro:' .$msg.'
							';	?>
    <script>
	window.location.replace("index.php");
	</script>
	<?php
    
}

include("rodape.php");