<?php
      require_once("conecta.php");



function inserirUnidade($conexao, $nome, $link, $descricao, $id_area, $dataDeProducao) {
    $query = "insert into unidade (nome, link, descricao, id_area,  dataDeProducao)
        values ('$nome', '$link', '$descricao', $id_area, '$dataDeProducao')";
    return mysqli_query($conexao, $query);
}

function listarUnidade($conexao) {
    $unidade = array();
	
		$query = 'SELECT unidade.id, unidade.nome, unidade.descricao, unidade.link, unidade.desativado, 
              area.nome AS areanome
              	FROM unidade, area
                  	WHERE area.id = unidade.id_area AND desativado = 0';
		
    $resultado = mysqli_query($conexao, $query);
	
    while($result = mysqli_fetch_assoc($resultado)) {
        array_push($unidade, $result);
    }
	
    return $unidade;
}

function buscarUnidade($conexao, $id) {
    $query = "select * from unidade where id = {$id}";
    $resultado = mysqli_query($conexao, $query);
    return mysqli_fetch_assoc($resultado);
}

function alterarUnidade($conexao, $id, $nome, $descricao, $dataDeProducao, $id_area) {
	date_default_timezone_set( 'America/Sao_Paulo' );
	$date = date( 'd-m-Y' );
	$hora = date( 'H:i:s' );
	
    $query = "update unidade set nome = '{$nome}', descricao = '{$descricao}', dataDeProducao = '{$dataDeProducao}', id_area = {$id_area}, datademodificacao = '$date $hora' where id = {$id}";
    return mysqli_query($conexao, $query);
}

function buscarUltimoId($conexao){
	$query = "SELECT id FROM unidade ORDER BY id DESC LIMIT 1";
    $resultado = mysqli_query($conexao, $query);
    return mysqli_fetch_assoc($resultado);
}

function removerUnidade($conexao, $id){
	$query = "update unidade set desativado = '1' where id = {$id}";
    return mysqli_query($conexao, $query);
}
