<?php
require_once( "cabecalho.php" );
require_once( "logica-usuario.php" );

//var_dump( $usuario );
/*Tratando a data*/
//echo $dataDeProducao = str_replace( '/', '', $dataDeProducao );
$id = $_GET['id'];

if($usuario['nivel'] == 1 ){
	$user = array();
	$user = listarUsuario( $conexao, $id );
}else{
	$user = $usuario;
}
?>

<div id="test-swipe-2" class="white padding15">
	<div class="row">

		<form action="usuario.php?acao=alterar" method="post" class="text-left">
			<input class="hide" name="id" value="<?= $user['id'];?>"/>
			<div class="col s12 m8">
				<label for="exampleInputNome"> Nome Completo:</label>
				<input class="form-control" id="exampleInputNome" type="text" name="nome" value="<?= $user['nome'];?>"/>
			</div>

			<div class="col s12 m4">
				<label for="exampleInputapelido"> Apelido:</label>
				<input class="form-control" id="exampleInputapelido" type="text" name="apelido" value="<?= $user['apelido'];?>"/>
			</div>

			<div class="col s12">
				<label for="exampleInputemail"> E-mail:</label>
				<input class="form-control" id="exampleInputemail" type="email" name="email" value="<?= $user['email'];?>"/>
			</div>			

			<div class="col s3">
				<input class="form-control" id="exampleInputemail" type="email" value="<?= $user['datadecriacao'];?>" disabled/>
			</div>

			<button class="btn btn-primary btn-block" type="submit">Alterar</button>
		</form>
	</div>
</div>

<?php include("rodape.php"); ?>