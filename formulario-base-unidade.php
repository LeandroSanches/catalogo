<div class="row">
	<div class="input-field col s12 m9">
		<input placeholder="Ex.: O Papel da Administração de Materiais na Organização" id="nome" type="text" class="validate" value="<?=$unidade['nome']?>" name="nome">
		<label for="nome">Nome</label>
	</div>

	<div class="input-field col s12 m3">
		<input id="dataDeProducao" name="dataDeProducao" type="text" class="validate" value="<?=$unidade['datadeproducao']?>" data-mask="00/00/0000">
		<label for="dataDeProducao">Data de Produção</label>
	</div>

	<?php if($unidade['link'] != ""){?>
	<div class="input-field col s12 m6">
		<input disabled placeholder="Ex.: https://catalogo.unigranrio/nomeDaDisciplina" id="link" type="text" class="validate" value="<?=$unidade['link']?>" name="link">
		<label for="link">Link</label>
	</div>
	<?php } ?>
	
	<?php if($unidade['datadecriacao'] != ""){?>
	<div class="input-field col s12 m3">
		<input disabled  id="datadecriacao" name="datadecriacao" type="text" class="validate" value="<?=$unidade['datadecriacao']?>" data-mask="0000/00/00 00:00:00">
		<label for="datadecriacao">Data de Criação</label>
	</div>
	<?php } ?>
	
	<?php if($unidade['datademodificacao'] != ""){?>
	<div class="input-field col s12 m3">
		<input disabled  id="datademodificacao" name="datademodificacao" type="text" class="validate" value="<?=$unidade['datademodificacao']?>" data-mask="00/00/0000">
		<label for="datademodificacao">Data de Modificação</label>
	</div>
	<?php } ?>

	<div class="input-field col s12">
		<textarea id="descricao" class="materialize-textarea" placeholder="Ex.: Esta unidade trata dos assuntos ... " name="descricao">
			<?=$unidade['descricao']?>
		</textarea>
		<label for="descricao">Descrição</label>
	</div>

</div>

<div class="row">
	<div class="col s12 m6">
		<label>Area</label>
		<select name="id_area" id="id_area">
			<option selected> </option>
			<?php foreach($area as $area) :
				$essaEhAArea = $unidade['id_area'] == $area['id'];
				$selecao = $essaEhAArea ? "selected='selected'" : "";
			?>
			<option value="<?=$area['id']?>" <?=$selecao?>>
				<?=$area['nome']?>
			</option>
			<?php endforeach ?>
		</select>

	</div>
</div>