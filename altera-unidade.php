<?php require_once("cabecalho.php");
      require_once("banco-unidade.php");

$id = $_POST["id"];
$nome = $_POST["nome"];
$link = $_POST["link"];
$descricao = $_POST["descricao"];
$dataDeProducao = $_POST['dataDeProducao'];
$id_area = $_POST['id_area'];

/*Tratando a data*/
echo $dataDeProducao = str_replace('/', '', $dataDeProducao);


if(alterarUnidade($conexao, $id, $nome, $descricao, $dataDeProducao, $id_area)) { ?>
    <p class="text-success">O produto <?= $nome; ?>, alterado com sucesso!</p>
<?php 
	$_SESSION["success"] = "Disciplina alterada com sucesso."; 
	header("Location: index.php"); 
} else {
    $msg = mysqli_error($conexao);
?>
    <p class="text-danger">O produto <?= $nome; ?> não foi alterado: <?= $msg ?></p>
<?php
}
?>

<?php include("rodape.php"); ?>
