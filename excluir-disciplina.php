<?php require_once("cabecalho.php");
      require_once("banco-disciplina.php");
      require_once("logica-usuario.php");

verificarUsuario();

$id = $_GET["id"];

if(removerDisciplina($conexao, $id)) { 
	$_SESSION["success"] = 'O Unidade foi Removido com sucesso!';	?>
    <script>
	window.location.replace("index.php");
	</script>
	<?php
	} else {
    $msg = mysqli_error($conexao);
	
	$_SESSION["success"] = '
								Atenção: O Unidade <strong>não</strong> foi Removido !<br>
								Erro:' .$msg.'
							';	?>
    <script>
	window.location.replace("index.php");
	</script>
	<?php
    
}

include("rodape.php");