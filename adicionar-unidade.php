<?php
header( "Content-Type: text/html; charset=ISO-8859-1", true );

require_once( "banco-unidade.php" );
require_once( "logica-usuario.php" );


verificarUsuario();

$id = buscarUltimoId( $conexao );
$id = ++$id[ 'id' ];
 						
$strlen = strlen($id);							

switch ($strlen) {
	case 1:
		$id = "UND_000".$id;
		break;
	case 2:
		$id = "UND_00".$id;
		break;
	case 3:
		$id = "UND_0".$id;
		break;
	case 4:
		$id = "UND_".$id;
		break;
}
echo( $id );


$nome = $_POST[ "nome" ];
$descricao = $_POST[ "descricao" ];
$id_area = $_POST[ 'id_area' ];
$dataDeProducao = $_POST[ "dataDeProducao" ];
$upload = 0;


/*Tratando a data*/
echo $dataDeProducao = str_replace('/', '', $dataDeProducao);


if ( $s = 'n' ) {
	/*subindo o arquivo a partir daqui*/
	ini_set( 'upload_max_filesize', '100M' );
	ini_set( 'post_max_size', '100M' );
	ini_set( 'max_input_time', 30000 );
	ini_set( 'max_execution_time', 30000 );

	include( "class.upload.php" );

	$myUpload = new Upload( $_FILES[ "arquivo" ] );

	$verificar = $myUpload->makeUpload();

	$nomezip = $_FILES[ "arquivo" ][ "name" ];

	$zip = new ZipArchive();

	$caminhozip = 'unidade/_zip/' . $nomezip;

	$caminhounzip = 'unidade/' . $id;

	/* criar zip
	if( $zip->open( $caminho, ZipArchive::CREATE )  === true){

		$zip->addFile(  'cursos/lorem.txt' , 'lorem.txt' );

		$zip->addFile(  'cursos/not_today.jpg' , 'cursos/not_today.jpg') ;

		$zip->addFromString('string.txt' , "Uma string qualquer" );

		$zip->close();
	}*/

	if ( $zip->open( $caminhozip ) === true ) {

		header( "Content-Type: text/html; charset=ISO-8859-1", true );

		$zip->extractTo( $caminhounzip );

		$zip->close();

		$upload = 1;
	}
	/*Subiu o arquivo*/

	/*Descobrino o caminho*/
	$unidade = explode( strrchr( $nomezip, "." ), $nomezip );

	$link = $caminhounzip . '/index.html';

	if ( inserirUnidade( $conexao, utf8_encode( $nome ), utf8_encode( $link ), utf8_encode( $descricao ), $id_area, $dataDeProducao ) ) {
		$_SESSION[ "success" ] = '<div class="msg padding15 teal lighten-4">A Unidade foi adicionado com sucesso!</div>';
		header("Location: adicionar.php");
	} else {
		$msg = mysqli_error( $conexao );

		$_SESSION[ "success" ] = '<div class="msg padding15 red darken-1 white-text "><p>Aten��o: A Unidade <strong>n�o</strong> foi adicionada !</p>' . $msg . '</div>';
		echo $msg;
		header("Location: adicionar.php");
	}
}